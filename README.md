# Aplicación de Web Service - Perfil de Facebook 

Esta aplicación es un web service que retorna la información global de un usuario de facebook, dependiendo del identificador enviado, realizado con SLIMPHP 3.

## Instalar Aplicación

Clonar la aplicación en el directorio del web server.

Una vez clonado debes correr composer para descargar todas las librerias.

    php composer.phar install

* Ten en cuenta que la carpeta `logs/` tiene permisos de escritura.

Para poder generar las estadisticas de consulta por usuario se debe crear el siguiente esquema en una base de datos MySQL

    CREATE SCHEMA `facebookprofileapp` DEFAULT CHARACTER SET utf8 ;

    CREATE TABLE IF NOT EXISTS `facebookprofileapp`.`request` (
    `user_id` BIGINT(20) NOT NULL,
    `ip_address` VARCHAR(45) NOT NULL,
    `date` DATETIME NOT NULL,
    `email` VARCHAR(350) NULL DEFAULT NULL,
    PRIMARY KEY (`user_id`, `date`))
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

    INSERT INTO request (user_id, ip_address, email, date) VALUES (1185370124893298, ":1", "andres@hotmail.com", NOW());

Debe configurar en el archivo settings.php los datos de la base de datos propia.

# Utilización

Una vez configurado, para poder recuperar los datos del perfil de un usuario de facebook debe utilizar

    http://localhost/facebookProfileApp/public/facebook/profile/{id}

* {id} debe ser cambiado por el id del usuario a buscar

También se puede recuperar estadisticas de la cantidad de veces que han buscado a un usuario

    http://localhost/facebookProfileApp/public/facebook/stats

Si desea ver la cantidad de veces con fechas que han buscado a un usuario por id puede utilizar

    http://localhost/facebookProfileApp/public/facebook/stats/byid/{id}

* {id} debe ser cambiado por el id del usuario a buscar

Si desea ver la cantidad de veces con fechas que han buscado a un usuario por email puede utilizar

    http://localhost/facebookProfileApp/public/facebook/stats/byemail/{email}

* {email} debe ser cambiado por el email del usuario a buscar
