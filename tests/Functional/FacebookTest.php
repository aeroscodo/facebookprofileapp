<?php

namespace Tests\Functional;

class FacebookTest extends BaseTestCase
{
    /**
     * Probar que devuelva que la pagina no existe si no envia ningun parámetro
     */
    public function testGetUserWithoutId()
    {
        $response = $this->runApp('GET', '/facebook/profile/');

        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Probar que devuelva error si ingresa el id con errores
     */
    public function testGetUserWithIdError()
    {
        $response = $this->runApp('GET', '/facebook/profile/a');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('error', (string)$response->getBody());
    }

    /**
     * Probar que es devuelto correctamente la peticion con un id correcto
     */
    public function testGetUserWithIdAllowed()
    {
        $response = $this->runApp('GET', '/facebook/profile/1185370124893298');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotContains('error', (string)$response->getBody());
    }
}