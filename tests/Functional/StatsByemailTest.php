<?php

namespace Tests\Functional;

class StatsByemailTest extends BaseTestCase
{
    /**
     * Probar que devuelva que la pagina no existe si no envia ningun parámetro
     */
    public function testGetStatsByemailWithoutEmail()
    {
        $response = $this->runApp('GET', '/facebook/stats/byemail/');

        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Probar que devuelva error si ingresa el email inexistente
     */
    public function testGetStatsByemailWithEmailError()
    {
        $response = $this->runApp('GET', '/facebook/stats/byemail/aaaaa@hotmail.com');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('error', (string)$response->getBody());
    }

    /**
     * Probar que es devuelto correctamente la peticion con un email correcto
     */
    public function testGetStatsByemailWithEmailAllowed()
    {
        $response = $this->runApp('GET', '/facebook/stats/byemail/andres@hotmail.com');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotContains('error', (string)$response->getBody());
    }
}