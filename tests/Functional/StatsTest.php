<?php

namespace Tests\Functional;

class StatsTest extends BaseTestCase
{
    /**
     * Probar que devuelva recuento de todos los ingresos de usuarios
     */
    public function testGetStatsWithoutId()
    {
        $response = $this->runApp('GET', '/facebook/stats');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotContains('error', (string)$response->getBody());
    }

    /**
     * Probar que devuelva pagina no encontrada si envia algun parametro
     */
    public function testGetStatsWithId()
    {
        $response = $this->runApp('GET', '/facebook/stats/111111111');

        $this->assertEquals(404, $response->getStatusCode());
    }
}