<?php

namespace Tests\Functional;

class StatsByidTest extends BaseTestCase
{
    /**
     * Probar que devuelva que la pagina no existe si no envia ningun parámetro
     */
    public function testGetStatsByidWithoutId()
    {
        $response = $this->runApp('GET', '/facebook/stats/byid/');

        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Probar que devuelva error si ingresa el id inexistente
     */
    public function testGetStatsByidWithIdError()
    {
        $response = $this->runApp('GET', '/facebook/stats/byid/11111111');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('error', (string)$response->getBody());
    }

    /**
     * Probar que es devuelto correctamente la peticion con un id correcto
     */
    public function testGetStatsByidWithIdAllowed()
    {
        $response = $this->runApp('GET', '/facebook/stats/byid/1185370124893298');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotContains('error', (string)$response->getBody());
    }
}