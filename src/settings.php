<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        
        //Database settings
        "db" => [
            "host" => "127.0.0.1",
            "dbname" => "facebookprofileapp",
            "user" => "root",
            "pass" => ""
        ],
        
        // Monolog settings
        'logger' => [
            'name' => 'facebookProfileApp',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        
        // Facebook settings
        'facebook' => [
            'path' => __DIR__ . '/../vendor/facebook/graph-sdk/src/Facebook/Facebook.php',
            'appId' => '1112011738911176',
            'secretKey' => '7d7319d41538620cc92068de812545a6',
            'defaultGraphVersion' => 'v2.8',
            'clientToken' => 'Fuf5iKRlvm8XSJmt9AoVaTRtzSc'
        ],
        
    ],
];
