<?php

$app->add(new RKA\Middleware\IpAddress());
// Routes
$app->get('/facebook/profile/{id}', function ($request, $response, $args) {

    $this->logger->info("FacebookProfileApp '/' Get user Profile");
    $id = $request->getAttribute('id');
    $response->withHeader('Content-type', 'application/json');
    try {
        // Solicitar a facebook el perfil del usuario
        $userRequest = $this->facebook->get('/'.$id.'?fields=id,link,name,first_name,middle_name,last_name,gender,email,age_range,relationship_status,birthday,cover,picture,about,website,hometown,work,religion,political,education,quotes,sports,favorite_athletes,favorite_teams,inspirational_people,interested_in,meeting_for,significant_other,accounts,achievements,books,currency,locale,location,timezone,devices,languages','1112011738911176|Fuf5iKRlvm8XSJmt9AoVaTRtzSc');
        if($userRequest) {
            $userProfile = json_decode($userRequest->getBody()); 
        } 
        // Insertar en la tabla el usuario solicitado
        $sql = "INSERT INTO request (user_id, ip_address, email, date) VALUES (:id, :ip_address, :email, NOW())";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $id);
        if(isset($userProfile->email))
            $sth->bindParam("email", $userProfile->email);
        else
            $sth->bindParam("email", 'undefined');
        $sth->bindParam("ip_address", $request->getAttribute('ip_address'));
        $sth->execute();
        
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      $this->logger->info("FacebookProfileApp '/' Facebook Profile not found");
      $userProfile = ['error'=>['message'=>$e->getMessage()]];
    } catch(PDOException $e) {
      $this->logger->info("FacebookProfileApp '/' Database Error");
      $userProfile = ['error'=>['message'=>$e->getMessage()]];
    }
    return $response->withJson($userProfile);
});

$app->get('/facebook/stats', function ($request, $response, $args) {
    $this->logger->info("FacebookProfileApp '/' Get stats");
    $response->withHeader('Content-type', 'application/json');
    try{
        $sth = $this->db->prepare("SELECT count(*) access, user_id, email FROM request GROUP BY user_id ORDER BY access desc");
        $sth->execute();
        $stats = $sth->fetchObject();
    } catch(PDOException $e) {
      $this->logger->info("FacebookProfileApp '/' Database Error - Stats");
      $stats = ['error'=>['message'=>$e->getMessage()]];
    }    
    return $response->withJson($stats);
});

$app->get('/facebook/stats/byid/{id}', function ($request, $response, $args) {
    $this->logger->info("FacebookProfileApp '/' Get stats by id");
    $response->withHeader('Content-type', 'application/json');
    $id = $request->getAttribute('id'); 
    try{
        $sth = $this->db->prepare("SELECT user_id, email, date FROM request WHERE user_id = :id ORDER BY date desc");
        $sth->bindParam("id", $id);
        $sth->execute();
        $stats = $sth->fetchObject();
        if(!$stats){
            $stats = ['error'=>['message'=>'El id ingresado no existe en la base de datos.']];
        }
    } catch(PDOException $e) {
      $this->logger->info("FacebookProfileApp '/' Database Error - Stats by Id");
      $stats = ['error'=>['message'=>$e->getMessage()]];
    }    
    return $response->withJson($stats);
});

$app->get('/facebook/stats/byemail/{email}', function ($request, $response, $args) {
    $this->logger->info("FacebookProfileApp '/' Get stats by email");
    $response->withHeader('Content-type', 'application/json');
    $email = $request->getAttribute('email'); 
    try{
        $sth = $this->db->prepare("SELECT user_id, email, date FROM request WHERE email = :email ORDER BY date desc");
        $sth->bindParam("email", $email);
        $sth->execute();
        $stats = $sth->fetchObject();
        if(!$stats){
            $stats = ['error'=>['message'=>'El email ingresado no existe en la base de datos.']];
        }
    } catch(PDOException $e) {
      $this->logger->info("FacebookProfileApp '/' Database Error - Stats by email");
      $stats = ['error'=>['message'=>$e->getMessage()]];
    }    
    return $response->withJson($stats);
});
